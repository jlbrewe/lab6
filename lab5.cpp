/*
  Jessica Brewer
  jlbrewe
  Lab 5
  Lab Section: 003
  TA: Nushrat Humaira
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   //inilialize loop variable and card array
   int i = 0;
   Card cardDeck[52];

   //for loop to create standard deck of cards
   //13 of each suit type and values 2-14 for each suit
   for (i = 0; i < 52; i++){
     if (i < 13){
       cardDeck[i].suit = SPADES;
     } else if (i < 26){
       cardDeck[i].suit = HEARTS;
     } else if (i < 39){
       cardDeck[i].suit = DIAMONDS;
     } else if (i < 52){
       cardDeck[i].suit = CLUBS;
     }
     cardDeck[i].value = (i%13) + 2;
   }


  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   //use random_shuffle function to shuffle the standard deck of cards
   random_shuffle(&cardDeck[0], &cardDeck[52], myrandom);
   //random_shuffle(cardDeck, cardDeck+52);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

    //assign hand array with first five values in the cardDeck array
    Card hand[5] = {cardDeck[0], cardDeck[1], cardDeck[2], cardDeck[3], cardDeck[4]};


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //use sort function to sort the newly formed hand of five cards
     sort(&hand[0], &hand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     //output to the user the randomly selected and sorted five cards
     //width of 10 and right aligned
     for (i = 0; i < 5; i++){
       //use the get_card_name and get_suit_code functions to display the right values/strings
       cout << setw(10) << right << get_card_name(hand[i]) << " of " << get_suit_code(hand[i]) << endl;
     }




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  //if left card suit value is smaller than right card suit value, return true
  if (lhs.suit < rhs.suit){
    return true;
    //if the suits equal
  } else if (lhs.suit == rhs.suit){
      //if left card value is smaller than right card value, return true, else return false
      if (lhs.value < rhs.value){
        return true;
      } else {
        return false;
      }
  //if left card suit value is bigger than right card suit value, return false
  } else {
    return false;
  }
}

string get_suit_code(Card& c) {
  //switch statement to return symbols
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  //create new string varaible
  string newString = "";
  //switch statement to return corect string based on assigned card value
  switch (c.value) {
    case 11:    return "Jack";
    case 12:    return "King";
    case 13:  return "Queen";
    case 14:     return "Ace";
    //if value is between 2-10, return that value in string form
    default:        return newString = to_string(c.value);
  }
}
